package src.main;
import java.io.*;
import java.util.*;

class WordSearchSolver {
    public void solveWordSearch(String filePath) throws FileNotFoundException {

        // 1. Create a file object to represent the input file.
        File file = new File(filePath);

        // 2. Create a scanner object that will read and parse from the file.
        Scanner scanner = new Scanner(file);

        // 3. Create an array to hold the integer dimensions by reading the first line, using 'x' as the delimiter.
        String[] dimensions = scanner.nextLine().split("x");

        // 4. Get the number of rows and columns from the dimensions array by converting the string values to ints.
        int rows = Integer.parseInt(dimensions[0]);
        int cols = Integer.parseInt(dimensions[1]);

        // 5. Initialize a 2D grid to represent the characters in the word search.
        char[][] searchGrid = new char[rows][cols];

        // 6. Populate the characters in the 2D grid with a nested for-loop.
        for (int i = 0; i < rows; i++) {
            // 6a. Read the current row of characters, remove the spaces, and store into a string.
            String row = scanner.nextLine().replace(" ", "");
            // 6b. Assign each character in the string to the corresponding position in the grid.
            for (int j = 0; j < cols; j++) { searchGrid[i][j] = row.charAt(j); }
        }

        // 7. Initialize a list to store the words that are to be found.
        List<String> wordsToFind = new ArrayList<>();

        // 8. While there are still words in file, add each one to the list.
        while (scanner.hasNext()) { wordsToFind.add(scanner.next()); }

        // 9. Create a new string 'word' for each iteration of word to be found.
        for (String word : wordsToFind) {

            // 9a. Go through each index of the grid.
            for (int r = 0; r < rows; r++) { for (int c = 0; c < cols; c++) {

                    // 9b. If the current index is not the first character of the word, check the next index.
                    if (searchGrid[r][c] != word.charAt(0)) { continue; }

                    // 9c. Check if characters fit horizontally, then search. If found, print out word and coordinates.
                    if (c + word.length() <= cols && checkHorizontally(searchGrid, word, r, c))
                    { System.out.println(word + " " + r + ":" + c + " " + r + ":" + (c + word.length() - 1)); }

                    // 9d. Check if characters fit backwards, then search. If found, print out word and coordinates.
                    if (c - word.length() + 1 >= 0 && checkHorizontallyBackwards(searchGrid, word, r, c))
                    { System.out.println(word + " " + r + ":" + c + " " + r + ":" + (c - word.length() + 1)); }

                    // 9e. Check if characters fit downwards, then search. If found, print out word and coordinates.
                    if (r + word.length() <= rows && checkDownwards(searchGrid, word, r, c))
                    { System.out.println(word + " " + r + ":" + c + " " + (r + word.length() - 1) + ":" + c); }

                    // 9f. Check if characters fit upwards, then search. If found, print out word and coordinates.
                    if (r - word.length() + 1 >= 0 && checkUpwards(searchGrid, word, r, c))
                    { System.out.println(word + " " + r + ":" + c + " " + (r - word.length() + 1) + ":" + c); }

                    // 9g. Check if characters fit diagonally downwards, then search. If found, print out word and coordinates.
                    if (r + word.length() <= rows && c + word.length() <= cols && checkDiagonallyDownwards(searchGrid, word, r, c))
                    { System.out.println(word + " " + r + ":" + c + " " + (r + word.length() - 1) + ":" + (c + word.length() - 1)); }

                    // 9h. Check if characters fit diagonally downwards (backwards), then search. If found, print out word and coordinates.
                    if (r - word.length() >= -1 && c - word.length() >= -1 && checkDiagonallyDB(searchGrid, word, r, c))
                    { System.out.println(word + " " + r + ":" + c + " " + (r - word.length() + 1) + ":" + (c - word.length() + 1)); }

                    // 9i. Check if characters fit diagonally upwards, then search. If found, print out word and coordinates.
                    if (r - word.length() + 1 >= 0 && c + word.length() <= cols && checkDiagonallyUpwards(searchGrid, word, r, c))
                    { System.out.println(word + " " + r + ":" + c + " " + (r - word.length() + 1) + ":" + (c + word.length() - 1)); }

                    // 9j. Check if characters fit diagonally upwards (backwards), then search. If found, print out word and coordinates.
                    if (r + word.length() <= rows && c - word.length() + 1 >= 0 && checkDiagonallyUB(searchGrid, word, r, c))
                    { System.out.println(word + " " + r + ":" + c + " " + (r + word.length() - 1) + ":" + (c - word.length() + 1)); }
                }
            }
        }
    }

    // The remaining functions below are boolean helper functions that search accordingly.
    // They will return true if each letter in the certain direction of the grid matches the word it's looking for.

    private boolean checkHorizontally(char[][] grid, String word, int row, int col) {
        for (int i = 0; i < word.length(); i++) {
            if (grid[row][col+i] != word.charAt(i))
            { return false; }
        } return true; }

    private boolean checkDownwards(char[][] grid, String word, int row, int col) {
        for (int i = 0; i < word.length(); i++) {
            if (grid[row+i][col] != word.charAt(i))
            { return false; }
        } return true; }

    private boolean checkHorizontallyBackwards(char[][] grid, String word, int row, int col) {
        for (int i = 0; i < word.length(); i++) {
            if (grid[row][col-i] != word.charAt(i))
            { return false; }
        } return true; }

    private boolean checkUpwards(char[][] grid, String word, int row, int col) {
        for (int i = 0; i < word.length(); i++) {
            if (grid[row-i][col] != word.charAt(i))
            { return false; }
        } return true; }

    private boolean checkDiagonallyDownwards(char[][] grid, String word, int row, int col) {
        for (int i = 0; i < word.length(); i++) {
            if (grid[row+i][col+i] != word.charAt(i))
            { return false; }
        } return true; }

    private boolean checkDiagonallyDB(char[][] grid, String word, int row, int col) {
        for (int i = 0; i < word.length(); i++) {
            if (grid[row-i][col-i] != word.charAt(i))
            { return false; }
        } return true; }

    private boolean checkDiagonallyUpwards(char[][] grid, String word, int row, int col) {
        for (int i = 0; i < word.length(); i++) {
            if (grid[row - i][col + i] != word.charAt(i))
            { return false; }
        } return true; }

    private boolean checkDiagonallyUB(char[][] grid, String word, int row, int col) {
        for (int i = 0; i < word.length(); i++) {
            if (grid[row + i][col - i] != word.charAt(i))
            { return false; }
        } return true; }
}