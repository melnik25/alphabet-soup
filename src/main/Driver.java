package src.main;
import java.io.FileNotFoundException;

public class Driver {
    public static void main(String[] args) throws FileNotFoundException {

        // 1. Create an instance of WordSearchSolver.
        WordSearchSolver solver = new WordSearchSolver();

        //2. Specify the text file here.
        solver.solveWordSearch("5x5");
    }
}
